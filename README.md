## Golang Docker Image with Oracle Supported

### Supported Versions
 - Golang 1.11
 - Oracle 11.2
 - Ora.v4 Golang Library 

### Oracle Golang Support on OSX

1) Install Oracle Instant-Client with `Basic` with `SDK` 
   (for RI 11.2) https://www.oracle.com/technetwork/topics/intel-macsoft-096467.html 
   (for other versions: https://www.oracle.com/technetwork/database/database-technologies/instant-client/downloads/index.html) 
2) Extract to basic and sdk zip files in `~/instantclient_11_2` folder 
3) For Connection Hostname should be added in /etc/hosts file like `127.0.0.1 hostname`
4) create `oci8.pc` file in `/usr/lib/pkgconfig` folder 
    ```oci8.pc
       ##### oci8.pc #####
       
       prefix=~/instantclient_11_2
       
       version=11.2
       build=client64
       
       libdir=${prefix}
       includedir=${prefix}/sdk/include
       
       Name: oci8
       Description: Oracle database engine
       Version: ${version}
       Libs: -L${libdir} -lclntsh -lnnz11
       Libs.private:
       Cflags: -I${includedir}
   ```
5) `-lnnz11` argument for 11.2 if you use different version please remove that.
6) For testing please run `go get gopkg.in/rana/ora.v4` command
7) Please check usage details in `https://github.com/rana/ora` repository
